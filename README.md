# Crowspath.org Tree Guide
https://crowspath.org/natural-history/trees/species-profiles/

# Running
``

# Deploying
TBD

# Developing
This project uses [Poetry](https://python-poetry.org/) to manage build, dependencies, and dev tools.
This project uses Python 3, which can be downloaded [here](https://www.python.org/downloads/)

You can download poetry with
```shell
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | $(PYTHON) -
```

And uninstall with
```shell
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | $(PYTHON) - --uninstall
```

## Adding a dependency
To add a python module `foobar`, use the following command `poetry add foobar`.
Remove with `poetry remove foobar`.

Update dependencies with `poetry lock`

## Running
Run with `poetry install` or `poetry shell`

## Versions
Change version with `poetry version 0.0.0`