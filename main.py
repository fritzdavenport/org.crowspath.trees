import csv
import os

from jinja2 import Template

OUTPUT_FILE = "dist/index.html"
OUTPUT_DIR = "/".join(OUTPUT_FILE.split("/")[:-1])
INPUT_FILE = "resources/trees.csv"
TEMPLATE_FILE = "resources/tree.html.jinja2"


def main():
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    with open(INPUT_FILE, 'r') as input_file, \
         open(TEMPLATE_FILE, 'r') as template_file, \
         open(OUTPUT_FILE, 'w') as output_file:

        trees = list(csv.DictReader(input_file))
        template = template_file.read()
        template_to_render = Template(template, autoescape=True)
        rendered_template = template_to_render.render(trees=trees)
        output_file.write(rendered_template)


if __name__ == "__main__":
    main()
